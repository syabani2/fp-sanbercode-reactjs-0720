import React, { useState } from 'react';
import axios from 'axios';
import { useHistory } from 'react-router-dom';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import EditIcon from '@material-ui/icons/Edit';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(3),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
    root: {
        width: '100%',
        '& > * + *': {
            marginTop: theme.spacing(2),
        },
    },
}));

export default function GameEdit() {
    const [inputName, setInputName] = useState('')
    const [inputGenre, setInputGenre] = useState('')
    const [inputSinglePlayer, setInputSinglePlayer] = useState('')
    const [inputMultiPlayer, setInputMultiPlayer] = useState('')
    const [inputPlatform, setInputPlatform] = useState('')
    const [inputImageUrl, setInputImageUrl] = useState('')
    const [inputRelease, setInputRelease] = useState('')
    const classes = useStyles();
    const history = useHistory()

    const handleSubmit = (event) => {
        event.preventDefault()

        axios.post(`https://backendexample.sanbersy.com/api/games`, { name: inputName, genre: inputGenre, singlePlayer: inputSinglePlayer, multiplayer: inputMultiPlayer, platform: inputPlatform, release: inputRelease, image_url: inputImageUrl })
            .then(res => {
                console.log(res)
                alert('Add Success')
                history.push("/manage")
            })
    }

    const handleChange = (event) => {
        switch(event.target.id) {
            case 'name':
                setInputName(event.target.value);
                break;
            case 'singleplayer':
                setInputSinglePlayer(event.target.value);
                break;
            case 'multiplayer':
                setInputMultiPlayer(event.target.value);
                break;
            case 'platform':
                setInputPlatform(event.target.value);
                break;
            case 'genre':
                setInputGenre(event.target.value);
                break;
            case 'image':
                setInputImageUrl(event.target.value);
                break;
            case 'release':
                setInputRelease(event.target.value);
                break;
            default:
                break;
        }
    }

    return (
        <Container component="main" maxWidth="xs">
            <CssBaseline />
            <div className={classes.paper}>
                <Avatar className={classes.avatar}>
                    <EditIcon />
                </Avatar>
                <Typography component="h1" variant="h5">
                    Add Games
                </Typography>
                <form className={classes.form} onSubmit={handleSubmit}>
                <Grid container spacing={2}>
                            <Grid item xs={12}>
                                <TextField
                                    variant="outlined"
                                    required
                                    fullWidth
                                    value={inputName}
                                    id="name"
                                    label="Name"
                                    name="name"
                                    size="small"
                                    onChange={handleChange}
                                    autoFocus
                                />
                            </Grid>
                            <Grid item xs={12} sm={4}>
                                <TextField
                                    name="release"
                                    variant="outlined"
                                    required
                                    fullWidth
                                    value={inputRelease}
                                    id="release"
                                    onChange={handleChange}
                                    label="Release"
                                    type="number"
                                    size="small"
                                />
                            </Grid>
                            <Grid item xs={12} sm={4}>
                                <TextField
                                    variant="outlined"
                                    required
                                    fullWidth
                                    value={inputSinglePlayer}
                                    id="singleplayer"
                                    onChange={handleChange}
                                    label="SinglePlayer"
                                    type="number"
                                    name="singleplayer"
                                    size="small"
                                />
                            </Grid>
                            <Grid item xs={12} sm={4}>
                                <TextField
                                    name="multiplayer"
                                    variant="outlined"
                                    required
                                    fullWidth
                                    value={inputMultiPlayer}
                                    id="multiplayer"
                                    onChange={handleChange}
                                    label="Multiplayer"
                                    type="number"
                                    size="small"
                                />
                            </Grid>
                            <Grid item xs={6}>
                                <TextField
                                    variant="outlined"
                                    required
                                    fullWidth
                                    name="genre"
                                    value={inputGenre}
                                    label="Genre"
                                    type="genre"
                                    id="genre"
                                    onChange={handleChange}
                                    size="small"
                                />
                            </Grid>
                            <Grid item xs={6}>
                                <TextField
                                    variant="outlined"
                                    fullWidth
                                    value={inputImageUrl}
                                    name="image"
                                    label="Image Url"
                                    id="image"
                                    onChange={handleChange}
                                    size="small"
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    variant="outlined"
                                    fullWidth
                                    name="platform"
                                    value={inputPlatform}
                                    label="Platform"
                                    id="platform"
                                    onChange={handleChange}
                                    size="small"
                                />
                            </Grid>
                        </Grid>
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                    >
                        Edit
                    </Button>
                </form>
            </div>
        </Container>
    );
}