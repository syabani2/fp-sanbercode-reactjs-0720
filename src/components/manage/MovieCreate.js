import React, { useState } from 'react';
import axios from 'axios';
import { useHistory } from 'react-router-dom';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import EditIcon from '@material-ui/icons/Edit';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(3),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
    root: {
        width: '100%',
        '& > * + *': {
            marginTop: theme.spacing(2),
        },
    },
}));

export default function MovieEdit() {
    const [inputTitle, setInputTitle] = useState('')
    const [inputYear, setInputYear] = useState('')
    const [inputDuration, setInputDuration] = useState('')
    const [inputRating, setInputRating] = useState('')
    const [inputGenre, setInputGenre] = useState('')
    const [inputImageUrl, setInputImageUrl] = useState('')
    const [inputDescription, setInputDescription] = useState('')
    const [inputReview, setInputReview] = useState('')
    const classes = useStyles();
    const history = useHistory()

    const handleSubmit = (event) => {
        event.preventDefault()

        axios.post(`https://backendexample.sanbersy.com/api/movies`, { title: inputTitle, description: inputDescription, year: inputYear, duration: inputDuration, genre: inputGenre, rating: inputRating, review: inputReview, image_url: inputImageUrl })
            .then(res => {
                console.log(res)
                alert('Add Success')
                history.push("/manage")
            })
    }

    const handleChange = (event) => {
        switch(event.target.id) {
            case 'title':
                setInputTitle(event.target.value);
                break;
            case 'year':
                setInputYear(event.target.value);
                break;
            case 'duration':
                setInputDuration(event.target.value);
                break;
            case 'rating':
                setInputRating(event.target.value);
                break;
            case 'genre':
                setInputGenre(event.target.value);
                break;
            case 'image':
                setInputImageUrl(event.target.value);
                break;
            case 'description':
                setInputDescription(event.target.value);
                break;
            case 'review':
                setInputReview(event.target.value);
                break;
            default:
                break;
        }
    }

    return (
        <Container component="main" maxWidth="xs">
            <CssBaseline />
            <div className={classes.paper}>
                <Avatar className={classes.avatar}>
                    <EditIcon />
                </Avatar>
                <Typography component="h1" variant="h5">
                    Add Movies
                </Typography>
                <form className={classes.form} onSubmit={handleSubmit}>
                    <Grid container spacing={2}>
                        <Grid item xs={12}>
                            <TextField
                                variant="outlined"
                                required
                                fullWidth
                                value={inputTitle}
                                id="title"
                                label="Title"
                                name="title"
                                size="small"
                                onChange={handleChange}
                                autoFocus
                            />
                        </Grid>
                        <Grid item xs={12} sm={4}>
                            <TextField
                                name="year"
                                variant="outlined"
                                required
                                fullWidth
                                value={inputYear}
                                type="number"
                                id="year"
                                onChange={handleChange}
                                label="Year"
                                size="small"
                            />
                        </Grid>
                        <Grid item xs={12} sm={4}>
                            <TextField
                                variant="outlined"
                                required
                                fullWidth
                                value={inputDuration}
                                id="duration"
                                onChange={handleChange}
                                label="Duration"
                                type="number"
                                name="duration"
                                size="small"
                            />
                        </Grid>
                        <Grid item xs={12} sm={4}>
                            <TextField
                                name="rating"
                                variant="outlined"
                                required
                                fullWidth
                                value={inputRating}
                                id="rating"
                                onChange={handleChange}
                                label="Rating"
                                type="number"
                                size="small"
                            />
                        </Grid>
                        <Grid item xs={6}>
                            <TextField
                                variant="outlined"
                                required
                                fullWidth
                                name="genre"
                                value={inputGenre}
                                label="Genre"
                                type="genre"
                                id="genre"
                                onChange={handleChange}
                                size="small"
                            />
                        </Grid>
                        <Grid item xs={6}>
                            <TextField
                                variant="outlined"
                                fullWidth
                                value={inputImageUrl}
                                name="image"
                                label="Image Url"
                                id="image"
                                onChange={handleChange}
                                size="small"
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                variant="outlined"
                                fullWidth
                                name="description"
                                value={inputDescription}
                                label="Description"
                                id="description"
                                onChange={handleChange}
                                size="small"
                                multiline
                                rowsMax={3}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                variant="outlined"
                                fullWidth
                                name="review"
                                value={inputReview}
                                label="Review"
                                id="review"
                                onChange={handleChange}
                                size="small"
                                multiline
                                rowsMax={3}
                            />
                        </Grid>
                    </Grid>
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                    >
                        Edit
                    </Button>
                </form>
            </div>
        </Container>
    );
}