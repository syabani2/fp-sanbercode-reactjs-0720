import React from 'react'
import ManageMovieList from './ManageMovieList'
import { MovieProvider } from '../content/MovieContext'

const ManageMovie = () => {
    return (
        <MovieProvider>
            <ManageMovieList />
        </MovieProvider>
    )
}

export default ManageMovie
