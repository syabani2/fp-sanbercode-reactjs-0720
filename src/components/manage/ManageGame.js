import React from 'react'
import ManageGameList from './ManageGameList'
import { GameProvider } from '../content/GameContext'

const ManageGame = () => {
    return (
        <GameProvider>
            <ManageGameList />
        </GameProvider>
    )
}

export default ManageGame
