import React, { useContext } from 'react';
import axios from 'axios';
import { makeStyles } from '@material-ui/core/styles';
import { useHistory } from 'react-router-dom';
import { Link } from 'react-router-dom';
import { LoginContext } from '../login/LoginContext';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(3),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));

export default function SignUpForm() {
    const history = useHistory();
    const classes = useStyles();
    const {userContext, usernameContext, passwordContext} = useContext(LoginContext);
    const [dataUser, setDataUser] = userContext;
    const [inputUsername, setInputUsername] = usernameContext;
    const [inputPassword, setInputPassword] = passwordContext;

    const handleSubmit = (event) => {
        event.preventDefault()
        let today = new Date();
        let inputCreatedAt = `${today.getFullYear()}-${(today.getMonth()+1)}-${+today.getDate()} ${today.getHours()}:${today.getMinutes()}:${today.getSeconds()}`;

        let checkUser = dataUser.filter(function(user) {
            return user.username === inputUsername && user.password === inputPassword;
        })

        if(checkUser.length !== 0) {
            alert('Username sudah ada, gunakan username yang lain!!')
            history.push("/register")
            setInputUsername("")
            setInputPassword("")
        } else {
            axios.post(`https://backendexample.sanbersy.com/api/users`, { created_at: inputCreatedAt, updated_at: inputCreatedAt, username: inputUsername, password: inputPassword })
                    .then(res => {
                        setDataUser([...dataUser, {id: res.data.id,  created_at: inputCreatedAt, updated_at: inputCreatedAt, username: inputUsername, password: inputPassword  }])
                    })

            setInputUsername("")
            setInputPassword("")

            setTimeout(function() {
                alert('Username berhasil ditambahkan!!')
            }, 200)

            setTimeout(function() {
                history.push("/login");
            }, 2000);
        }
    }

    const handleChangeUsername = (event) => {
        setInputUsername(event.target.value)
    }
    const handleChangePassword = (event) => {
        setInputPassword(event.target.value)
    }

    return (
        <Container component="main" maxWidth="xs">
            <CssBaseline />
            <div className={classes.paper}>
                <Avatar className={classes.avatar}>
                    <LockOutlinedIcon />
                </Avatar>
                <Typography component="h1" variant="h5">
                    Sign up
        </Typography>
                <form className={classes.form} onSubmit={handleSubmit}>
                    <Grid container spacing={2}>
                        <Grid item xs={12}>
                            <TextField
                                variant="outlined"
                                required
                                fullWidth
                                id="username"
                                label="Username"
                                name="username"
                                autoFocus
                                value={inputUsername}
                                onChange={handleChangeUsername}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                variant="outlined"
                                required
                                fullWidth
                                name="password"
                                label="Password"
                                type="password"
                                id="password"
                                value={inputPassword}
                                onChange={handleChangePassword}
                            />
                        </Grid>
                    </Grid>
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                    >
                        Sign Up
                    </Button>
                    <Grid container justify="flex-end">
                        <Grid item>
                            <Link to="/login">Already have an account? Sign in</Link>
                        </Grid>
                    </Grid>
                </form>
            </div>
        </Container>
    );
}