import React, { useContext } from 'react';
import { useHistory } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';
import { LoginContext } from './LoginContext';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';

const formStyle = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    }
}));

export default function LoginForm() {
    const history = useHistory();
    const formClasses = formStyle();
    const { userContext, usernameContext, passwordContext } = useContext(LoginContext);
    const [dataUser,] = userContext;
    const [inputUsername, setInputUsername] = usernameContext;
    const [inputPassword, setInputPassword] = passwordContext;

    const handleSubmit = (event) => {
        event.preventDefault()
        let checkUser = dataUser.filter(function(user) {
            return user.username === inputUsername && user.password === inputPassword;
        })

        if (checkUser.length === 0) {
            alert('Username Tidak ditemukan!')
            setInputUsername("")
            setInputPassword("")
            history.push("/login")
        } else {
            localStorage.setItem('status', 1)
            localStorage.setItem('idUser', checkUser[0]['id'])
            localStorage.setItem('username', checkUser[0]['username'])
            alert('Login Berhasil!')
            history.push("/home");
            window.location.reload(false);
        }
    }

    const handleChangeUsername = (event) => {
        setInputUsername(event.target.value)
    }
    const handleChangePassword = (event) => {
        setInputPassword(event.target.value)
    }

    return (
        <Container component="main" maxWidth="xs">
            <CssBaseline />
            <div className={formClasses.paper}>
                <Avatar className={formClasses.avatar}>
                    <LockOutlinedIcon />
                </Avatar>
                <Typography component="h1" variant="h5">
                    Sign in
                </Typography>
                <form className={formClasses.form} onSubmit={handleSubmit}>
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        id="username"
                        label="Username"
                        name="username"
                        value={inputUsername}
                        onChange={handleChangeUsername}
                        autoFocus
                    />
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        name="password"
                        label="Password"
                        type="password"
                        id="password"
                        value={inputPassword}
                        onChange={handleChangePassword}
                    />
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={formClasses.submit}
                    >
                        Sign In
                    </Button>
                    <Grid container>
                        <Grid item>
                            <Link to="/register">{"Don't have an account? Sign Up"}</Link>
                        </Grid>
                    </Grid>
                </form>
            </div>
        </Container>
    );
}