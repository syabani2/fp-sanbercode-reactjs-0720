import React, { useState, useEffect, createContext } from 'react';
import axios from 'axios';

export const LoginContext = createContext();

export const LoginProvider = props => {
    const [dataUser, setDataUser] = useState(null)
    const [inputUsername, setInputUsername] = useState("")
    const [inputPassword, setInputPassword] = useState("")
    const [selectedId, setSelectedId] = useState(0)
    const [statusForm, setStatusFrom] = useState("login")

    useEffect(() => {
        if(dataUser === null) {
            axios.get('https://backendexample.sanbersy.com/api/users')
                .then(res => {
                    setDataUser(res.data.map(el => { return { id: el.id, username: el.username, password: el.password }}))
                })
        }
    }, [dataUser])

    return (
        <LoginContext.Provider value={
            {
                userContext: [dataUser, setDataUser],
                usernameContext: [inputUsername, setInputUsername],
                passwordContext: [inputPassword, setInputPassword],
                idContext: [selectedId, setSelectedId],
                formContext: [statusForm, setStatusFrom]
            }
        }>
            {props.children}
        </LoginContext.Provider>
    )
}