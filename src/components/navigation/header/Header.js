import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Link, useHistory } from 'react-router-dom';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogTitle from '@material-ui/core/DialogTitle';

const headerStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
    },
    button: {
        color: 'white',
        textDecoration: 'none',
        '&:hover': {
            border: '1px solid #E90046',
            textDecoration: 'none'
        }
    },
    link: {
        textDecoration: 'none'
    },
    name: {
        marginLeft: '20px',
        fontSize: '14px'
    }
}));


const Header = () => {
    const [open, setOpen] = React.useState(false);

    const statusLogin = localStorage.getItem('status')
    const username = localStorage.getItem('username')

    const history = useHistory();
    const headerClasses = headerStyles();


    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const handleLogOut = () => {
        setOpen(false);
        localStorage.setItem('status', 0)
        localStorage.setItem('idUser', 0)
        localStorage.setItem('username', '')
        history.push("/login");
        window.location.reload(false);
    }

    console.log(statusLogin)
    return (
        <div className={headerClasses.root}>
            <AppBar position="static">
                <Toolbar>
                    <Typography variant="h6" className={headerClasses.title}>
                        Syaban's Web
                    </Typography>

                    {
                        parseInt(statusLogin) === 1 ? (
                            <>
                                <Link to="/home" className={headerClasses.link}>
                                    <Button color="inherit" className={headerClasses.button}>Home</Button>
                                </Link>
                                <Link to="/manage" className={headerClasses.link}>
                                    <Button color="inherit" className={headerClasses.button}>Manage</Button>
                                </Link>
                                <Link to="/change/password" className={headerClasses.link}>
                                    <Button color="inherit" className={headerClasses.button} >Change Password</Button>
                                </Link>
                                <Button color="inherit" className={headerClasses.button} onClick={handleClickOpen} >Log Out</Button>
                                <Typography className={headerClasses.name}>
                                    Hello, {username}
                                </Typography>
                            </>
                        ) : (
                            <>
                                <Link to="/home" className={headerClasses.link}>
                                    <Button color="inherit" className={headerClasses.button}>Home</Button>
                                </Link>
                                <Link to="/login" className={headerClasses.link}>
                                    <Button color="inherit" className={headerClasses.button}>Sign in</Button>
                                </Link>
                            </>
                        )
                    }

                    <Dialog
                        open={open}
                        onClose={handleClose}
                        aria-labelledby="alert-dialog-title"
                        aria-describedby="alert-dialog-description"
                    >
                        <DialogTitle id="alert-dialog-title">{"Are you sure you want to logout?"}</DialogTitle>
                        <DialogActions>
                            <Button onClick={handleClose} color="primary">
                                No
                            </Button>
                            <Button onClick={handleLogOut} color="primary" autoFocus>
                                Yes
                            </Button>
                        </DialogActions>
                    </Dialog>
                </Toolbar>
            </AppBar>
        </div>
    );
}

export default Header