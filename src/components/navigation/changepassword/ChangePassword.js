import React, { useContext, useState } from 'react';
import axios from 'axios';
import { makeStyles } from '@material-ui/core/styles';
import { useHistory } from 'react-router-dom';
import { LoginContext } from '../login/LoginContext';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        width: '500px'
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(3),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
    button: {
        marginBottom: '10px'
    }
}));

export default function ChangePassword() {
    const history = useHistory();
    const classes = useStyles();
    const {userContext, passwordContext} = useContext(LoginContext);
    const [dataUser, setDataUser] = userContext;
    const [inputPassword, setInputPassword] = passwordContext;
    const [inputOldPassword, setInputOldPassword] = useState('')

    let idUser = localStorage.getItem('idUser')
    let user = dataUser.find(x => x.id === parseInt(idUser))

    const handleSubmit = (event) => {
        event.preventDefault()
        let today = new Date();
        let inputUpdatedAt = `${today.getFullYear()}-${(today.getMonth()+1)}-${+today.getDate()} ${today.getHours()}:${today.getMinutes()}:${today.getSeconds()}`;

        if(user.password === inputOldPassword) {
            axios.put(`https://backendexample.sanbersy.com/api/users/${idUser}`, { updated_at: inputUpdatedAt, username: user.username, password: inputPassword })
                    .then(res => {
                        user.updated_at = inputUpdatedAt
                        user.password = inputPassword
                        setDataUser([...dataUser])
                    })
            setTimeout(function() {
                alert('Password Berhasil diupdate')
            }, 200)

            setTimeout(function() {
                localStorage.setItem('status', 0)
                localStorage.setItem('idUser', 0)
                localStorage.setItem('username', '')
                history.push("/login");
                window.location.reload(false);
            }, 2000);
        } else {
            alert('Password Salah!')
            setInputOldPassword('')
            setInputPassword('')
        }
    }

    const handleChangePassword = (event) => {
        switch(event.target.id) {
            case 'password':
                setInputPassword(event.target.value);
                break;
            case 'oldpassword':
                setInputOldPassword(event.target.value);
                break;
            default:
                break;
        }
    }

    return (
        <Container component="main" maxWidth="xs">
            <CssBaseline />
            <div className={classes.paper}>
                <Avatar className={classes.avatar}>
                    <LockOutlinedIcon />
                </Avatar>
                <Typography component="h1" variant="h5">
                    Change Password for User {user.username}
                </Typography>
                <form className={classes.form} onSubmit={handleSubmit}>
                    <Grid container spacing={2}>
                        <Grid item xs={12}>
                            <TextField
                                    variant="outlined"
                                    required
                                    fullWidth
                                    name="oldpassword"
                                    label="Old Password"
                                    type="password"
                                    id="oldpassword"
                                    value={inputOldPassword}
                                    onChange={handleChangePassword}
                                    className={classes.button}
                                />
                            <TextField
                                variant="outlined"
                                required
                                fullWidth
                                name="password"
                                label="New Password"
                                type="password"
                                id="password"
                                value={inputPassword}
                                onChange={handleChangePassword}
                            />
                        </Grid>
                    </Grid>
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                    >
                        Change Password
                    </Button>
                    <Grid container justify="flex-end">
                    </Grid>
                </form>
            </div>
        </Container>
    );
}