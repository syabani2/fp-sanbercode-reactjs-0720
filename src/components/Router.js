import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Header from './navigation/header/Header';
import Footer from './footer/Footer';
import Login from './navigation/login/Login';
import Register from './navigation/register/Register';
import Home from './content/Home';
import ChangePassword from './navigation/changepassword/ChangePassword';
import ReviewMovie from './content/ReviewMovie';
import ReviewGame from './content/ReviewGame';
import Manage from './manage/Manage';
import MovieCreate from './manage/MovieCreate';
import GameCreate from './manage/GameCreate';
import MovieEdit from './manage/MovieEdit';
import GameEdit from './manage/GameEdit';

const Routes = () => {
    const statusLogin = localStorage.getItem('status')
    console.log(statusLogin)
    return (
        parseInt(statusLogin) === 1 ? (
            <>
                <Header />
                <Switch>
                    <Route path="/home">
                        <Home />
                    </Route>
                    <Route path="/change/password">
                        <ChangePassword />
                    </Route>
                    <Route path="/movie/review/:id" component={ReviewMovie} />
                    <Route path="/game/review/:id" component={ReviewGame} />
                    <Route path="/movie/edit/:id" component={MovieEdit} />
                    <Route path="/game/edit/:id" component={GameEdit} />
                    <Route path="/manage">
                        <Manage />
                    </Route>
                    <Route path="/movie/create">
                        <MovieCreate />
                    </Route>
                    <Route path="/game/create">
                        <GameCreate />
                    </Route>
                </Switch>
                <Footer />
            </>
        ) : (
            <>
                <Header />
                <Switch>
                    <Route path="/login">
                        <Login />
                    </Route>
                    <Route path="/register">
                        <Register />
                    </Route>
                    <Route path="/home">
                        <Home />
                    </Route>
                    <Route path="/movie/review/:id" component={ReviewMovie} />
                    <Route path="/game/review/:id" component={ReviewGame} />
                </Switch>
                <Footer />
            </>
        )
    )
}

export default Routes
