import React, { Component } from 'react';
import { withStyles } from '@material-ui/styles';
import PropTypes from 'prop-types';
import axios from 'axios';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import './Review.css';

const styles = theme => ({
    root: {
        marginTop: '20px',
        margin: 'auto',
        maxWidth: 605,
        maxHeight: 700,
        overflowY: 'scroll'
    },
    image: {
        objectFit: 'fill',
        position: 'sticky',
        top: 0
    },
    textHead: {
        marginTop: '20px',
    }
});

class ReviewMovie extends Component {
    constructor({ match }) {
        super();
        this.state = {
            id: match.params.id,
            movies: {}
        };
        console.log(this.state.id)
    }

    componentDidMount() {
        axios.get(`https://backendexample.sanbersy.com/api/movies/${this.state.id}`)
            .then(res => {
                let tempMovies = res.data;
                this.setState({ movies: tempMovies })
            })
    }

    render() {
        const { classes } = this.props;
        return (
            <>
                <h1 className={classes.textHead}>Review '{this.state.movies.title}'</h1>
                <Card className={classes.root}>
                    <CardActionArea>
                        <CardMedia
                            component="img"
                            alt="Contemplative Reptile"
                            height="500"
                            image={this.state.movies.image_url}
                            title="Contemplative Reptile"
                            className={classes.image}
                        />
                        <CardContent>
                            <Typography gutterBottom variant="h5" component="h2">
                                {this.state.movies.title}
                            </Typography>
                            <Typography variant="body2" color="textSecondary" component="p">
                                <b>Year</b>: {this.state.movies.year} | <b>Duration</b>: {this.state.movies.duration} Menit | <b>Rating</b>: {this.state.movies.rating}
                            </Typography>
                            <Typography variant="body2" color="textSecondary" component="p">
                                <b>Genre</b>: {this.state.movies.genre}
                            </Typography>
                            <Typography variant="body2" color="textSecondary" component="p">
                                <hr/ ><br />
                                <b>Description</b>: <br/>
                                {this.state.movies.description}
                            </Typography>
                            <Typography variant="body2" color="textSecondary" component="p">
                                <hr/ ><br />
                                <b>Review</b>: <br/>
                                {this.state.movies.review}
                            </Typography>
                        </CardContent>
                    </CardActionArea>
                </Card>
            </>
        );
    }
}

ReviewMovie.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ReviewMovie);