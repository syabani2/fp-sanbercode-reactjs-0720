import React, { useState, useEffect, createContext } from 'react';
import axios from 'axios';

export const MovieContext = createContext();

export const MovieProvider = props => {
    const [dataMovie, setDataMovie] = useState(null)
    const [inputTitle, setInputTitle] = useState("")
    const [inputDescription, setInputDescription] = useState("")
    const [inputYear, setInputYear] = useState("")
    const [inputDuration, setInputDuration] = useState("")
    const [inputGenre, setInputGenre] = useState("")
    const [inputRating, setInputRating] = useState("")
    const [inputReview, setInputReview] = useState("")
    const [inputImageUrl, setInputImageUrl] = useState("")
    const [selectedId, setSelectedId] = useState(0)
    const [statusForm, setStatusFrom] = useState("login")

    useEffect(() => {
        if(dataMovie === null) {
            axios.get('https://backendexample.sanbersy.com/api/movies')
                .then(res => {
                    setDataMovie(res.data.map(el => { return { id: el.id, created_at: el.created_at, updated_at: el.updated_at, title: el.title, description: el.description, year: el.year, duration: el.duration, genre: el.genre, rating: el.rating, review: el.review, image_url: el.image_url }}))
                })
        }
    }, [dataMovie])

    return (
        <MovieContext.Provider value={
            {
                dataContext: [dataMovie, setDataMovie],
                titleContext: [inputTitle, setInputTitle],
                descContext: [inputDescription, setInputDescription],
                yearContext: [inputYear, setInputYear],
                durationContext: [inputDuration, setInputDuration],
                genreContext: [inputGenre, setInputGenre],
                ratingContext: [inputRating, setInputRating],
                reviewContext: [inputReview, setInputReview],
                imageContext: [inputImageUrl, setInputImageUrl],
                idContext: [selectedId, setSelectedId],
                formContext: [statusForm, setStatusFrom]
            }
        }>
            {props.children}
        </MovieContext.Provider>
    )
}