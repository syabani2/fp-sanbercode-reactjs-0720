import React, { useState, useEffect, createContext } from 'react';
import axios from 'axios';

export const GameContext = createContext();

export const GameProvider = props => {
    const [dataGame, setDataGame] = useState(null)
    const [inputName, setInputName] = useState("")
    const [inputGenre, setInputGenre] = useState("")
    const [inputSinglePlayer, setInputSinglePlayer] = useState("")
    const [inputMultiplayer, setInputMultiplayer] = useState("")
    const [inputPlatform, setInputPlatform] = useState("")
    const [inputRelease, setInputRelease] = useState("")
    const [inputImageUrl, setInputImageUrl] = useState("")
    const [selectedId, setSelectedId] = useState(0)
    const [statusForm, setStatusFrom] = useState("login")

    useEffect(() => {
        if(dataGame === null) {
            axios.get('https://backendexample.sanbersy.com/api/games')
                .then(res => {
                    setDataGame(res.data.map(el => { return { id: el.id, created_at: el.created_at, updated_at: el.updated_at, name: el.name, genre: el.genre, singlePlayer: el.singlePlayer, multiplayer: el.multiplayer, platform: el.platform, release: el.release, image_url: el.image_url }}))
                })
        }
    }, [dataGame])

    return (
        <GameContext.Provider value={
            {
                dataContext: [dataGame, setDataGame],
                nameContext: [inputName, setInputName],
                genreContext: [inputGenre, setInputGenre],
                singleContext: [inputSinglePlayer, setInputSinglePlayer],
                multiContext: [inputMultiplayer, setInputMultiplayer],
                platformContext: [inputPlatform, setInputPlatform],
                releaseContext: [inputRelease, setInputRelease],
                imageContext: [inputImageUrl, setInputImageUrl],
                idContext: [selectedId, setSelectedId],
                formContext: [statusForm, setStatusFrom]
            }
        }>
            {props.children}
        </GameContext.Provider>
    )
}