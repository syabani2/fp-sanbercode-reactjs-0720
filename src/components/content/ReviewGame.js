import React, { Component } from 'react';
import { withStyles } from '@material-ui/styles';
import PropTypes from 'prop-types';
import axios from 'axios';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import './Review.css';

const styles = theme => ({
    root: {
        marginTop: '20px',
        margin: 'auto',
        maxWidth: 605,
        maxHeight: 700,
        overflowY: 'scroll'
    },
    image: {
        objectFit: 'fill',
        position: 'sticky',
        top: 0
    },
    textHead: {
        marginTop: '20px',
    }
});

class ReviewGame extends Component {
    constructor({ match }) {
        super();
        this.state = {
            id: match.params.id,
            games: {}
        };
        console.log(this.state.id)
    }

    componentDidMount() {
        axios.get(`https://backendexample.sanbersy.com/api/games/${this.state.id}`)
            .then(res => {
                let tempGames = res.data;
                this.setState({ games: tempGames })
            })
    }

    render() {
        const { classes } = this.props;
        return (
            <>
                <h1 className={classes.textHead}>Review '{this.state.games.name}'</h1>
                <Card className={classes.root}>
                    <CardActionArea>
                        <CardMedia
                            component="img"
                            alt="Contemplative Reptile"
                            height="500"
                            image={this.state.games.image_url}
                            title="Contemplative Reptile"
                            className={classes.image}
                        />
                        <CardContent>
                            <Typography gutterBottom variant="h5" component="h2">
                                {this.state.games.name}
                            </Typography>
                            <Typography variant="body2" color="textSecondary" component="p">
                                <b>Release</b>: {this.state.games.release} | <b>Platform</b>: {this.state.games.platform}
                            </Typography>
                            <Typography variant="body2" color="textSecondary" component="p">
                                <b>Genre</b>: {this.state.games.genre}
                            </Typography>
                            <Typography variant="body2" color="textSecondary" component="p">
                                <b>SinglePlayer</b>: {this.state.games.singlePlayer} | <b>MultiPlayer</b>: {this.state.games.multiplayer}
                            </Typography>
                        </CardContent>
                    </CardActionArea>
                </Card>
            </>
        );
    }
}

ReviewGame.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ReviewGame);