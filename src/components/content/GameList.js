import React, { useContext, useState } from 'react';
import axios from 'axios';
import { GameContext } from './GameContext';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import { Link } from 'react-router-dom';
import './Game.css';

const formStyle = makeStyles((theme) => ({
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    textField: {
        marginRight: '10px',
        width: '150px'
    },
    note: {
        fontSize: '12px',
        color: 'grey',
        marginBottom: '5px',
        fontStyle: 'italic'
    },
    button: {
        marginBottom: '7px',
        display: 'block'
    },
    link: {
        textDecoration: 'none'
    },
}));

export default function GameList() {
    const { dataContext } = useContext(GameContext)
    const [dataGame, setDataGame] = dataContext
    const [inputName, setInputName] = useState('')
    const [inputGenre, setInputGenre] = useState('')
    const [inputPlatform, setInputPlatform] = useState('')
    const [inputERelease, setInputERelease] = useState('')
    const [inputSRelease, setInputSRelease] = useState('')
    const formClasses = formStyle();
    const [, setColumn] = useState('')
    const [sort, setSort] = useState(0)

    const handleFilter = (event) => {
        event.preventDefault()

        axios.get('https://backendexample.sanbersy.com/api/games')
        .then(res => {
            let tempGames =  res.data.map(el => { return { id: el.id, created_at: el.created_at, updated_at: el.updated_at, name: el.name, genre: el.genre, singlePlayer: el.singlePlayer, multiplayer: el.multiplayer, platform: el.platform, release: el.release, image_url: el.image_url }})

            if(!inputName && !inputPlatform && !inputGenre && !inputERelease && !inputSRelease) {
                setDataGame([...tempGames])
            } else {
                if (inputName) {
                    tempGames = tempGames.filter(function(game) {
                        return game.name.toUpperCase().includes(inputName.toUpperCase());
                    })
                }

                if (inputSRelease && inputSRelease !== 0 && inputERelease && inputERelease !== 0 && inputSRelease <= inputERelease) {
                    tempGames = tempGames.filter(function(game) {
                        return game.release >= inputSRelease && game.release <= inputERelease;
                    })
                }

                if (inputPlatform) {
                    tempGames = tempGames.filter(function(game) {
                        return game.platform.toUpperCase().includes(inputPlatform.toUpperCase());
                    })
                }

                if (inputGenre) {
                    tempGames = tempGames.filter(function(game) {
                        return game.genre.toUpperCase().includes(inputGenre.toUpperCase());
                    })
                }
                setDataGame([...tempGames])
                console.log(dataGame)
            }
        })
    }

    const handleChange = (event) => {
        switch(event.target.name) {
            case 'name':
                setInputName(event.target.value)
                break;
            case 'erelease':
                setInputERelease(event.target.value)
                break;
            case 'srelease':
                setInputSRelease(event.target.value)
                break;
            case 'genre':
                setInputGenre(event.target.value)
                break;
            case 'platform':
                setInputPlatform(event.target.value)
                break;
            default:
                break;
        }
    }

    const handleSortBy = (event) => {
        setColumn(event.target.id)
        switch(event.target.id) {
            case 'name':
                if (sort === 0) {
                    setDataGame(dataGame.sort((a, b) => ((a.name === null ? a.name : a.name.toUpperCase()) < (b.name === null ? b.name : b.name.toUpperCase()) ? -1:1)))
                    console.log(dataGame)
                    setSort(1)
                } else {
                    setDataGame(dataGame.sort((a, b) => ((a.name === null ? a.name : a.name.toUpperCase()) < (b.name === null ? b.name : b.name.toUpperCase()) ? 1:-1)))
                    console.log(dataGame)
                    setSort(0)
                }
                break;
            case 'genre':
                if (sort === 0) {
                    setDataGame(dataGame.sort((a, b) => ((a.genre === null ? a.genre : a.genre.toUpperCase()) < (b.genre === null ? b.genre : b.genre.toUpperCase()) ? -1:1)))
                    console.log(dataGame)
                    setSort(1)
                } else {
                    setDataGame(dataGame.sort((a, b) => ((a.genre === null ? a.genre : a.genre.toUpperCase()) < (b.genre === null ? b.genre : b.genre.toUpperCase()) ? 1:-1)))
                    console.log(dataGame)
                    setSort(0)
                }
                break;
            case 'singleplayer':
                if (sort === 0) {
                    setDataGame(dataGame.sort((a, b) => (a.singlePlayer > b.singlePlayer ? -1:1)))
                    console.log(dataGame)
                    setSort(1)
                } else {
                    setDataGame(dataGame.sort((a, b) => (a.singlePlayer > b.singlePlayer ? 1:-1)))
                    console.log(dataGame)
                    setSort(0)
                }
                break;
            case 'multiplayer':
                if (sort === 0) {
                    setDataGame(dataGame.sort((a, b) => (a.multiplayer > b.multiplayer ? -1:1)))
                    console.log(dataGame)
                    setSort(1)
                } else {
                    setDataGame(dataGame.sort((a, b) => (a.multiplayer > b.multiplayer ? 1:-1)))
                    console.log(dataGame)
                    setSort(0)
                }
                break;
            case 'platform':
                if (sort === 0) {
                    setDataGame(dataGame.sort((a, b) => ((a.platform === null ? a.platform : a.platform.toUpperCase()) < (b.platform === null ? b.platform : b.platform.toUpperCase()) ? -1:1)))
                    console.log(dataGame)
                    setSort(1)
                } else {
                    setDataGame(dataGame.sort((a, b) => ((a.platform === null ? a.platform : a.platform.toUpperCase()) < (b.platform === null ? b.platform : b.platform.toUpperCase()) ? 1:-1)))
                    console.log(dataGame)
                    setSort(0)
                }
                break;
            case 'release':
                if (sort === 0) {
                    setDataGame(dataGame.sort((a, b) => (a.release > b.release ? -1:1)))
                    console.log(dataGame)
                    setSort(1)
                } else {
                    setDataGame(dataGame.sort((a, b) => (a.release > b.release ? 1:-1)))
                    console.log(dataGame)
                    setSort(0)
                }
                break;
            default:
                alert('none');
                break;
        }
    }

    const handleSort = (event) => {
        event.preventDefault()
    }

    return (
        <>
            <h1>Game List</h1>
            <form className={formClasses.form}  onSubmit={handleFilter}>
                    <TextField
                        className={formClasses.textField}
                        variant="outlined"
                        margin="normal"
                        id="name"
                        label="by Name"
                        name="name"
                        size="small"
                        onChange={handleChange}
                        value={inputName}
                        autoFocus
                    />
                    <TextField
                        className={formClasses.textField}
                        variant="outlined"
                        margin="normal"
                        id="genre"
                        label="by Genre"
                        name="genre"
                        size="small"
                        onChange={handleChange}
                        value={inputGenre}
                    />
                    <TextField
                        className={formClasses.textField}
                        variant="outlined"
                        margin="normal"
                        id="platform"
                        label="by Platform"
                        name="platform"
                        size="small"
                        onChange={handleChange}
                        value={inputPlatform}
                    />
                    <TextField
                        className={formClasses.textField}
                        variant="outlined"
                        margin="normal"
                        id="srelease"
                        label="SRelease"
                        name="srelease"
                        type="number"
                        size="small"
                        onChange={handleChange}
                        value={inputSRelease}
                    />
                    <TextField
                        className={formClasses.textField}
                        variant="outlined"
                        margin="normal"
                        id="erelease"
                        label="ERelease"
                        name="erelease"
                        type="number"
                        size="small"
                        onChange={handleChange}
                        value={inputERelease}
                    />
                    <Button size="small" type="submit" variant="outlined" color="primary" className={formClasses.button}>
                        Search
                    </Button>
                    <p className={formClasses.note}>*Empthy field and click Search for reset data</p>
            </form>
            <div className="container__table">
                <form onSubmit={handleSort}>
                    <table className="content">
                        <thead>
                            <tr>
                                <th className="table__header"><button onClick={handleSortBy} id="name">Name</button></th>
                                <th className="table__header"><button onClick={handleSortBy} id="genre">Genre</button></th>
                                <th className="table__header"><button onClick={handleSortBy} id="singleplayer">SinglePlayer</button></th>
                                <th className="table__header"><button onClick={handleSortBy} id="multiplayer">Multiplayer</button></th>
                                <th className="table__header"><button onClick={handleSortBy} id="platform">Platform</button></th>
                                <th className="table__header"><button onClick={handleSortBy} id="release">Release</button></th>
                                <th style={{ width: '100px' }}>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                dataGame !== null && dataGame.map((val, index) => {
                                    return (
                                        <tr key={index}>
                                            <td className="ellipsis">{val.name}</td>
                                            <td className="ellipsis">{val.genre}</td>
                                            <td className="ellipsis">{val.singlePlayer}</td>
                                            <td className="ellipsis">{val.multiplayer}</td>
                                            <td className="ellipsis">{val.platform}</td>
                                            <td className="ellipsis">{val.release}</td>
                                            <td style={{textAlign: "center"}}>
                                                <Link to={`game/review/${val.id}`} className={formClasses.link}>
                                                    <Button size="small" variant="contained" id={val.id} color="primary">
                                                        Review
                                                    </Button>
                                                </Link>
                                            </td>
                                        </tr>
                                    );
                                })
                            }
                        </tbody>
                    </table>
                </form>
            </div>
        </>
    );
}