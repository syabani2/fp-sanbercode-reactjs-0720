import React, { useContext, useState } from 'react';
import axios from 'axios';
import { MovieContext } from './MovieContext';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import { Link } from 'react-router-dom';
import './Movie.css';

const formStyle = makeStyles((theme) => ({
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    textField: {
        marginRight: '10px',
        width: '100px'
    },
    button: {
        marginBottom: '7px',
        display: 'block'
    },
    note: {
        fontSize: '12px',
        color: 'grey',
        marginBottom: '5px',
        fontStyle: 'italic'
    },
    link: {
        textDecoration: 'none'
    },
}));

export default function MovieList() {
    const { dataContext, titleContext } = useContext(MovieContext)
    const [dataMovie, setDataMovie] = dataContext
    const [inputTitle, setInputTitle] = titleContext
    const [inputSYear, setInputSYear] = useState('')
    const [inputSDuration, setInputSDuration] = useState('')
    const [inputSRating, setInputSRating] = useState('')
    const [inputEYear, setInputEYear] = useState('')
    const [inputEDuration, setInputEDuration] = useState('')
    const [inputERating, setInputERating] = useState('')
    const formClasses = formStyle();
    const [, setColumn] = useState('')
    const [sort, setSort] = useState(0)

    const handleFilter = (event) => {
        event.preventDefault()

        axios.get('https://backendexample.sanbersy.com/api/movies')
        .then(res => {
            let tempMovies =  res.data.map(el => { return { id: el.id, created_at: el.created_at, updated_at: el.updated_at, title: el.title, description: el.description, year: el.year, duration: el.duration, genre: el.genre, rating: el.rating, review: el.review, image_url: el.image_url }})

            if(!inputTitle && !inputEYear && !inputEDuration && !inputERating && !inputSYear && !inputSDuration && !inputSRating) {
                setDataMovie([...tempMovies])
            } else {
                if (inputTitle) {
                    tempMovies = tempMovies.filter(function(movie) {
                        return movie.title.toUpperCase().includes(inputTitle.toUpperCase());
                    })
                }

                if (inputSYear && inputSYear !== 0 && inputEYear && inputEYear !== 0 && inputSYear <= inputEYear) {
                    tempMovies = tempMovies.filter(function(movie) {
                        return movie.year >= inputSYear && movie.year <= inputEYear;
                    })
                }

                if (inputSDuration && inputSDuration !== 0 && inputEDuration && inputEDuration !== 0 && inputSDuration <= inputEDuration) {
                    tempMovies = tempMovies.filter(function(movie) {
                        return movie.duration >= inputSDuration && movie.duration <= inputEDuration;
                    })
                }

                if (inputSRating && inputSRating !== 0 && inputERating && inputERating !== 0) {
                    tempMovies = tempMovies.filter(function(movie) {
                        return movie.rating >= inputSRating && movie.rating <= inputERating;
                    })
                }
                setDataMovie([...tempMovies])
                console.log(dataMovie)
            }
        })
    }

    const handleChange = (event) => {
        switch(event.target.name) {
            case 'title':
                setInputTitle(event.target.value)
                break;
            case 'eyear':
                setInputEYear(event.target.value)
                break;
            case 'eduration':
                setInputEDuration(event.target.value)
                break;
            case 'erating':
                setInputERating(event.target.value)
                break;
            case 'syear':
                setInputSYear(event.target.value)
                break;
            case 'sduration':
                setInputSDuration(event.target.value)
                break;
            case 'srating':
                setInputSRating(event.target.value)
                break;
            default:
                break;
        }
    }

    const handleSortBy = (event) => {
        setColumn(event.target.id)
        switch(event.target.id) {
            case 'title':
                if (sort === 0) {
                    setDataMovie(dataMovie.sort((a, b) => ((a.title === null ? a.title : a.title.toUpperCase()) < (b.title === null ? b.title : b.title.toUpperCase()) ? -1:1)))
                    console.log(dataMovie)
                    setSort(1)
                } else {
                    setDataMovie(dataMovie.sort((a, b) => ((a.title === null ? a.title : a.title.toUpperCase()) < (b.title === null ? b.title : b.title.toUpperCase()) ? 1:-1)))
                    console.log(dataMovie)
                    setSort(0)
                }
                break;
            case 'desc':
                if (sort === 0) {
                    setDataMovie(dataMovie.sort((a, b) => ((a.description === null ? a.description : a.description.toUpperCase()) < (b.description === null ? b.description : b.description.toUpperCase()) ? -1:1)))
                    console.log(dataMovie)
                    setSort(1)
                } else {
                    setDataMovie(dataMovie.sort((a, b) => ((a.description === null ? a.description : a.description.toUpperCase()) < (b.description === null ? b.description : b.description.toUpperCase()) ? 1:-1)))
                    console.log(dataMovie)
                    setSort(0)
                }
                break;
            case 'year':
                if (sort === 0) {
                    setDataMovie(dataMovie.sort((a, b) => (a.year > b.year ? -1:1)))
                    console.log(dataMovie)
                    setSort(1)
                } else {
                    setDataMovie(dataMovie.sort((a, b) => (a.year > b.year ? 1:-1)))
                    console.log(dataMovie)
                    setSort(0)
                }
                break;
            case 'duration':
                if (sort === 0) {
                    setDataMovie(dataMovie.sort((a, b) => (a.duration > b.duration ? -1:1)))
                    console.log(dataMovie)
                    setSort(1)
                } else {
                    setDataMovie(dataMovie.sort((a, b) => (a.duration > b.duration ? 1:-1)))
                    console.log(dataMovie)
                    setSort(0)
                }
                break;
            case 'rating':
                if (sort === 0) {
                    setDataMovie(dataMovie.sort((a, b) => (a.rating > b.rating ? -1:1)))
                    console.log(dataMovie)
                    setSort(1)
                } else {
                    setDataMovie(dataMovie.sort((a, b) => (a.rating > b.rating ? 1:-1)))
                    console.log(dataMovie)
                    setSort(0)
                }
                break;
            case 'genre':
                if (sort === 0) {
                    setDataMovie(dataMovie.sort((a, b) => ((a.genre === null ? a.genre : a.genre.toUpperCase()) < (b.genre === null ? b.genre : b.genre.toUpperCase()) ? -1:1)))
                    console.log(dataMovie)
                    setSort(1)
                } else {
                    setDataMovie(dataMovie.sort((a, b) => ((a.genre === null ? a.genre : a.genre.toUpperCase()) < (b.genre === null ? b.genre : b.genre.toUpperCase()) ? 1:-1)))
                    console.log(dataMovie)
                    setSort(0)
                }
                break;
            case 'review':
                if (sort === 0) {
                    setDataMovie(dataMovie.sort((a, b) => ((a.review === null ? a.review : a.review.toUpperCase()) < (b.review === null ? b.review : b.review.toUpperCase()) ? -1:1)))
                    console.log(dataMovie)
                    setSort(1)
                } else {
                    setDataMovie(dataMovie.sort((a, b) => ((a.review === null ? a.review : a.review.toUpperCase()) < (b.review === null ? b.review : b.review.toUpperCase()) ? 1:-1)))
                    console.log(dataMovie)
                    setSort(0)
                }
                break;
            default:
                alert('none');
                break;
        }
    }

    const handleSort = (event) => {
        event.preventDefault()
    }

    return (
        <>
            <h1>Movie List</h1>
            <form className={formClasses.form} onSubmit={handleFilter}>
                    <TextField
                        className={formClasses.textField}
                        variant="outlined"
                        margin="normal"
                        id="title"
                        label="by Title"
                        name="title"
                        onChange={handleChange}
                        value={inputTitle}
                        size="small"
                        autoFocus
                    />
                    <TextField
                        className={formClasses.textField}
                        variant="outlined"
                        margin="normal"
                        id="syear"
                        label="SYear"
                        name="syear"
                        type="number"
                        onChange={handleChange}
                        value={inputSYear}
                        size="small"
                    />
                    <TextField
                        className={formClasses.textField}
                        variant="outlined"
                        margin="normal"
                        id="eyear"
                        label="EYear"
                        name="eyear"
                        type="number"
                        onChange={handleChange}
                        value={inputEYear}
                        size="small"
                    />
                    <TextField
                        className={formClasses.textField}
                        variant="outlined"
                        margin="normal"
                        id="sduration"
                        label="SDuration"
                        name="sduration"
                        type="number"
                        onChange={handleChange}
                        value={inputSDuration}
                        size="small"
                    />
                    <TextField
                        className={formClasses.textField}
                        variant="outlined"
                        margin="normal"
                        id="eduration"
                        label="EDuration"
                        name="eduration"
                        type="number"
                        onChange={handleChange}
                        value={inputEDuration}
                        size="small"
                    />
                    <TextField
                        className={formClasses.textField}
                        variant="outlined"
                        margin="normal"
                        id="srating"
                        label="SRating"
                        name="srating"
                        type="number"
                        onChange={handleChange}
                        value={inputSRating}
                        size="small"
                    />
                    <TextField
                        className={formClasses.textField}
                        variant="outlined"
                        margin="normal"
                        id="erating"
                        label="ERating"
                        name="erating"
                        type="number"
                        onChange={handleChange}
                        value={inputERating}
                        size="small"
                    />
                    <Button size="small" type="submit" variant="outlined" color="primary" className={formClasses.button}>
                        Search
                    </Button>
                    <p className={formClasses.note}>*Empthy field and click Search for reset data</p>
            </form>
            <div className="container__table">
                <form onSubmit={handleSort}>
                    <table className="content">
                        <thead>
                            <tr>
                                <th className="table__header"> <button onClick={handleSortBy} id="title">Title</button> </th>
                                <th className="table__header"> <button onClick={handleSortBy} id="desc">Desc</button> </th>
                                <th className="table__header"> <button onClick={handleSortBy} id="year">Year</button> </th>
                                <th className="table__header"> <button onClick={handleSortBy} id="duration">Duration</button> </th>
                                <th className="table__header"> <button onClick={handleSortBy} id="genre">Genre</button> </th>
                                <th className="table__header"> <button onClick={handleSortBy} id="rating">Rating</button> </th>
                                <th className="table__header"> <button onClick={handleSortBy} id="review">Review</button> </th>
                                <th style={{ width: '100px' }}>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                dataMovie !== null && dataMovie.map((val, index) => {
                                    return (
                                        <tr key={index}>
                                            <td className="ellipsis">{val.title}</td>
                                            <td className="ellipsis">{val.description}</td>
                                            <td className="ellipsis">{val.year}</td>
                                            <td className="ellipsis">{val.duration} Menit</td>
                                            <td className="ellipsis">{val.genre}</td>
                                            <td className="ellipsis">{val.rating}</td>
                                            <td className="ellipsis">{val.review}</td>
                                            <td style={{textAlign: "center"}}>
                                                <Link to={`movie/review/${val.id}`} className={formClasses.link}>
                                                    <Button size="small" variant="contained" id={val.id} color="primary">
                                                        Review
                                                    </Button>
                                                </Link>
                                            </td>
                                        </tr>
                                    );
                                })
                            }
                        </tbody>
                    </table>
                </form>
            </div>
        </>
    );
}