import React from 'react'
import MovieList from './MovieList'
import { MovieProvider } from './MovieContext'
import './Movie.css';

const Login = () => {
    return (
        <MovieProvider>
            <MovieList />
        </MovieProvider>
    )
}

export default Login
