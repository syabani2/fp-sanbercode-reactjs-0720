import React from 'react'
import GameList from './GameList'
import { GameProvider } from './GameContext'
import './Movie.css';

const Login = () => {
    return (
        <GameProvider>
            <GameList />
        </GameProvider>
    )
}

export default Login
