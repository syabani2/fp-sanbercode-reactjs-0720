import React from 'react';
import './App.css';
import { BrowserRouter as Router } from 'react-router-dom';
import Routes from './components/Router'
import { LoginProvider } from './components/navigation/login/LoginContext'

function App() {
  return (
    <div className="app">
      <Router>
        <LoginProvider>
          <Routes />
        </LoginProvider>
      </Router>
    </div>
  );
}

export default App;
